// COMMENTS in Javascript
// - To write comments, we use two forward slash for single line comments and two forward slash and two asterisks for multiple comments.
// This is a single line comment.
/*
	This is a multi-line comment.
	To write them, we add two asterisks inside of the 
	forward slashes and write comments in between line.
*/

// Variable
/*
	- Variables contain values that can be changed over the execution time of the program.
	- To declare a variable, we use the "let" keyboard.
*/

// let productName = 'desktop computer';
// console.log(productName);
// productName = 'cellphone';
// console.log(productName);

// let productPrice = 500;
// console.log(productPrice);
// productPrice = 450;
// console.log(productPrice);

// CONSTANTS
/*
	- Use constants for values that will not change.
*/

// const deliveryFee = 30;
// console.log(deliveryFee);

// DATA TYPES

// 1. STRINGS
/*
	- Strings are a series of characters that create a word, a phrase, sentence, or anything related to "TEXT".
	- Strings in JavaScript can be written using a single quote ('') or a double quote ("")
	- On other programming languages, only the double quote can be used for creating strings.
*/
let country = 'Philippines';
let province = "Metro Manila";

// CONCATENATION
console.log(country + ", " + province);

// NUMBERS
/*
	- Include integers/whole numbers, decimal numbers, fraction, exponential notation
*/

let headCount = 26;
console.log(headCount);

let grade = 98.7;
console.log(grade)

let planetDistance = 2e10;
console.log(planetDistance);

let PI = 22/7;
console.log(PI);

console.log(Math.PI);

console.log("John's grade last quarter is: " + grade);

// BOOLEAN
/*
	- Boolean values are logical values.
	- Boolean values can contain either "true" or "false"
*/
let isMarried = false;
let isGoodConduct = true;
console.log("isMarried: " + isMarried);
console.log("isGoodConduct: " + isGoodConduct);
console.log(isMarried);
console.log(isGoodConduct);

// OBJECTS

	// Arrays
	/*
		- They are a special kind of data that stores multiple values.
		- They are a special type of an object.
		- They can store different data types but is normally used to store similar data types.
	*/

	// Syntax:
		// let/const arrayName = [ElementA, ElementB, ElementC...]


	let grades = [ 98.7, 92.1, 90.2, 94.6];
	console.log(grades);
	console.log(grades[0]);

	let userDetails = ["John", "Smith", 32, true];
	console.log(userDetails);

	// Object Literals
	/*
		- Objects are another special kind of data type that mimics real world objects/items
		- They are used to create complex data that contains piecess of information that are relevant to each other.
		-Every individual piece of information if called a property of the object.

		Syntax:

			let/const objectName = {
				propertyA: value,
				propertyB; value,
			}
	*/ 

	let personDetails = {
		fullName: 'Juan Dela Cruz',
		age: 35,
		isMarried: false,
		contact: ['+639876543210', '024785425'],
		address: {
			houseNumber: '345',
			city: 'Manila'
		}
	}
	console.log(personDetails);